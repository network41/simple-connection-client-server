import socket

def main():
    host = '127.0.0.1'  # ใช้ localhost หรือ IP address ของเครื่องที่ต้องการเชื่อมต่อ server
    port = 12345       # ใช้ port ที่ server กำหนดไว้

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((host, port))

    while True:
        received_data = client_socket.recv(1024).decode('utf-8')
        if not received_data:
            break
        print("Server sent:", received_data)
        num = int(received_data) + 1
        data = str(num)
        client_socket.send(data.encode('utf-8'))

    client_socket.close()

if __name__ == '__main__':
    main()
